#### Tools
* Dependency Injection - Koin
* Navigation Component
* ViewBinding
---
#### Flow
##### View > ViewModel > UseCase > Repository
##### View < ViewModel < UseCase < Repository
---
#### 2 main screen
* WeatherFragment
* ForecastFragment
---
#### Technical Design
* 2 fragment screen sync unit between Celsius and Fahrenheit by MainActivity (fragment observe livedata from share viewmodel)
* load data when city text changed with debounce 1 sec
* load data when unit changed (switch between Celsius and Fahrenheit)
---
#### Unit test
##### ViewModel
* WeatherViewModelTest
* ForecastViewModelTest
##### UseCase
* LoadWeatherUseCaseTest
* LoadForecastUseCaseTest
##### Mapper
* WeatherToUiMapperTest
* ForecastToUiMapperTest
##### Repository
* I didn't do it. cause Repository is only called network service 
