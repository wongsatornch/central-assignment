package com.h2osolid.assignment

import com.h2osolid.assignment.core.interactor.CoroutinesDispatcherProvider
import com.h2osolid.assignment.core.model.Result
import com.h2osolid.assignment.domain.UseCase
import io.mockk.coEvery
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Assert

fun provideFakeCoroutinesDispatcherProvider(
    dispatcher: TestCoroutineDispatcher
): CoroutinesDispatcherProvider {
    return CoroutinesDispatcherProvider(dispatcher, dispatcher, dispatcher)
}

@Suppress("NOTHING_TO_INLINE")
inline fun <T> T.assertValue(expected: T): T {
    MatcherAssert.assertThat(
        "Value $this should be equal to $expected",
        this,
        CoreMatchers.equalTo(expected)
    )
    return this
}

inline fun <T> T.assertValue(block: (T) -> Boolean): T {
    Assert.assertTrue("Value $this does not match the predicate.", block(this))
    return this
}

fun <T : Any?> successResult(data: T): Result<T> = Result.Success(data)
fun <T : Any?> failResult(exception: Exception): Result<T> = Result.Failure(exception)

@Suppress("NOTHING_TO_INLINE")
inline fun <reified P : Any, R : Any?> UseCase<P, R>.mockReturnResult(crossinline answer: (P?) -> Result<R>) {
    coEvery { this@mockReturnResult(any()) } coAnswers {
        val param: P? = firstArg()
        answer(param)
    }
}