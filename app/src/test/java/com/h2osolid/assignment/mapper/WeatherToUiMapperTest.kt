package com.h2osolid.assignment.mapper

import com.h2osolid.assignment.model.Main
import com.h2osolid.assignment.model.WeatherResponse
import com.h2osolid.assignment.ui.model.mapper.WeatherToUiMapper
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class WeatherToUiMapperTest {

    private lateinit var underTest: WeatherToUiMapper

    @Before
    fun setup() {
        underTest = WeatherToUiMapper
    }

    @Test
    fun `test map weather to ui model`() {
        val uiModel = underTest.map(WeatherResponse(Main(0.0, 0), "bangkok"))

        assertThat("Result temp should be 0.0", uiModel.temp, equalTo("0.0"))
        assertThat("Result humidity should be 0", uiModel.humidity, equalTo("0"))
        assertThat("Result date should be current-date", uiModel.city, equalTo("bangkok"))
    }
}