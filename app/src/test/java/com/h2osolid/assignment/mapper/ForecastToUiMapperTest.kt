package com.h2osolid.assignment.mapper

import com.h2osolid.assignment.model.Forecast
import com.h2osolid.assignment.model.Main
import com.h2osolid.assignment.ui.model.mapper.ForecastToUiMapper
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Before
import org.junit.Test

class ForecastToUiMapperTest {

    private lateinit var underTest: ForecastToUiMapper

    @Before
    fun setup() {
        underTest = ForecastToUiMapper
    }

    @Test
    fun `test map forecast to ui model`() {
        val uiModel = underTest.map(Forecast(Main(0.0, 0), "bangkok", "current-date"))

        assertThat("Result temp should be 0.0", uiModel.temp, equalTo("0.0"))
        assertThat("Result humidity should be 0", uiModel.humidity, equalTo("0"))
        assertThat("Result date should be current-date", uiModel.date, equalTo("current-date"))
    }
}