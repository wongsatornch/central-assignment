package com.h2osolid.assignment.usecase

import com.h2osolid.assignment.assertValue
import com.h2osolid.assignment.domain.Repository
import com.h2osolid.assignment.domain.weather.LoadWeatherUseCase
import com.h2osolid.assignment.model.Units
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

class LoadWeatherUseCaseTest : BaseUseCaseTest() {

    private val repository: Repository = mockk(relaxed = true)
    private lateinit var underTest: LoadWeatherUseCase

    override fun setUpTest() {
        underTest = LoadWeatherUseCase(repository)
    }

    @Test
    fun `verify get weather success`() = runBlockingTest {
        val result = underTest(LoadWeatherUseCase.Params("bangkok", Units.CELSIUS))
        result.assertValue { it.isSuccess }
        coVerify(exactly = 1) { repository.getWeather(any(), any()) }
    }

    @Test
    fun `verify query is null`() = runBlockingTest {
        val result = underTest(LoadWeatherUseCase.Params(null, Units.CELSIUS))
        result.assertValue { it.isFailure }
        coVerify(exactly = 0) { repository.getWeather(any(), any()) }
    }

    @Test
    fun `verify query is empty`() = runBlockingTest {
        val result = underTest(LoadWeatherUseCase.Params(null, Units.CELSIUS))
        result.assertValue { it.isFailure }
        coVerify(exactly = 0) { repository.getWeather(any(), any()) }
    }

    @Test
    fun `verify unit is null`() = runBlockingTest {
        val result = underTest(LoadWeatherUseCase.Params("", null))
        result.assertValue { it.isFailure }
        coVerify(exactly = 0) { repository.getWeather(any(), any()) }
    }

    @Test
    fun `verify query and unit is null`() = runBlockingTest {
        val result = underTest(LoadWeatherUseCase.Params(null, null))
        result.assertValue { it.isFailure }
        coVerify(exactly = 0) { repository.getWeather(any(), any()) }
    }
}