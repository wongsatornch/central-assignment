package com.h2osolid.assignment.usecase

import com.h2osolid.assignment.CoroutinesTestRule
import org.junit.Before
import org.junit.Rule

abstract class BaseUseCaseTest {
    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Before
    fun setUp() {
        setUpTest()
    }

    protected abstract fun setUpTest()
}