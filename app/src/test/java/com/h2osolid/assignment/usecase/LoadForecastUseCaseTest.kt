package com.h2osolid.assignment.usecase

import com.h2osolid.assignment.assertValue
import com.h2osolid.assignment.domain.Repository
import com.h2osolid.assignment.domain.forecast.LoadForecastUseCase
import com.h2osolid.assignment.model.Units
import io.mockk.coVerify
import io.mockk.mockk
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Test

class LoadForecastUseCaseTest : BaseUseCaseTest() {

    private val repository: Repository = mockk(relaxed = true)
    private lateinit var underTest: LoadForecastUseCase

    override fun setUpTest() {
        underTest = LoadForecastUseCase(repository)
    }

    @Test
    fun `verify get forecast success`() = runBlockingTest {
        val result = underTest(LoadForecastUseCase.Params("bangkok", Units.CELSIUS))
        result.assertValue { it.isSuccess }
        coVerify(exactly = 1) { repository.getForecast(any(), any()) }
    }

    @Test
    fun `verify query is null`() = runBlockingTest {
        val result = underTest(LoadForecastUseCase.Params(null, Units.CELSIUS))
        result.assertValue { it.isFailure }
        coVerify(exactly = 0) { repository.getForecast(any(), any()) }
    }

    @Test
    fun `verify query is empty`() = runBlockingTest {
        val result = underTest(LoadForecastUseCase.Params("", Units.CELSIUS))
        result.assertValue { it.isFailure }
        coVerify(exactly = 0) { repository.getForecast(any(), any()) }
    }

    @Test
    fun `verify unit is null`() = runBlockingTest {
        val result = underTest(LoadForecastUseCase.Params("", null))
        result.assertValue { it.isFailure }
        coVerify(exactly = 0) { repository.getForecast(any(), any()) }
    }

    @Test
    fun `verify query and unit is null`() = runBlockingTest {
        val result = underTest(LoadForecastUseCase.Params(null, null))
        result.assertValue { it.isFailure }
        coVerify(exactly = 0) { repository.getForecast(any(), any()) }
    }
}