package com.h2osolid.assignment.viewmodel

import com.h2osolid.assignment.domain.forecast.LoadForecastUseCase
import com.h2osolid.assignment.failResult
import com.h2osolid.assignment.mockReturnResult
import com.h2osolid.assignment.model.Forecast
import com.h2osolid.assignment.model.ForecastResponse
import com.h2osolid.assignment.model.Main
import com.h2osolid.assignment.model.Units
import com.h2osolid.assignment.provideFakeCoroutinesDispatcherProvider
import com.h2osolid.assignment.successResult
import com.h2osolid.assignment.ui.forecast.ForecastViewModel
import com.h2osolid.assignment.ui.model.ForecastUiModel
import com.jraska.livedata.TestObserver
import com.jraska.livedata.test
import io.mockk.coVerify
import io.mockk.mockk
import org.junit.Test

class ForecastViewModelTest : BaseViewModelTest() {

    private val loadForecastUseCase: LoadForecastUseCase = mockk(relaxed = true)
    private lateinit var underTest: ForecastViewModel

    private lateinit var weathersObserver: TestObserver<List<ForecastUiModel>?>
    private lateinit var errorObserver: TestObserver<Throwable?>

    override fun setUpTest() {
        underTest = ForecastViewModel(
            loadForecastUseCase,
            provideFakeCoroutinesDispatcherProvider(coroutinesTestRule.testDispatcher)
        )
        weathersObserver = underTest.weathers.test()
        errorObserver = underTest.error.test()
    }

    @Test
    fun `verify get forecast success`() = coroutinesTestRule.runBlockingTest {
        loadForecastUseCase.mockReturnResult {
            successResult(ForecastResponse(listOf(Forecast(Main(0.0, 0), "city", "date"))))
        }
        underTest.unit = Units.CELSIUS
        underTest.loadForecast()

        errorObserver.assertNullValue()
        weathersObserver.assertHasValue()
        weathersObserver.assertValue(
            listOf(
                ForecastUiModel("0.0", "0", "date", Units.CELSIUS)
            )
        )
        coVerify(exactly = 1) { loadForecastUseCase(any()) }
    }


    @Test
    fun `verify failed`() = coroutinesTestRule.runBlockingTest {
        val exception = NullPointerException()
        loadForecastUseCase.mockReturnResult {
            failResult(exception)
        }
        underTest.unit = Units.CELSIUS
        underTest.loadForecast()

        weathersObserver.assertNullValue()
        errorObserver.assertHasValue()
        errorObserver.assertValue(exception)

        coVerify(exactly = 1) { loadForecastUseCase(any()) }
    }
}