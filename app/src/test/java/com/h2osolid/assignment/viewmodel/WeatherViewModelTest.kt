package com.h2osolid.assignment.viewmodel

import com.h2osolid.assignment.domain.weather.LoadWeatherUseCase
import com.h2osolid.assignment.failResult
import com.h2osolid.assignment.mockReturnResult
import com.h2osolid.assignment.model.Main
import com.h2osolid.assignment.model.Units
import com.h2osolid.assignment.model.WeatherResponse
import com.h2osolid.assignment.provideFakeCoroutinesDispatcherProvider
import com.h2osolid.assignment.successResult
import com.h2osolid.assignment.ui.model.WeatherUiModel
import com.h2osolid.assignment.ui.weather.WeatherViewModel
import com.jraska.livedata.TestObserver
import com.jraska.livedata.test
import io.mockk.coVerify
import io.mockk.mockk
import org.junit.Test

class WeatherViewModelTest : BaseViewModelTest() {

    private val loadWeatherUseCase: LoadWeatherUseCase = mockk(relaxed = true)
    private lateinit var underTest: WeatherViewModel

    private lateinit var weatherUiModelObserver: TestObserver<WeatherUiModel?>
    private lateinit var errorObserver: TestObserver<Throwable?>
    private lateinit var cityObserver: TestObserver<String?>

    override fun setUpTest() {
        underTest = WeatherViewModel(
            loadWeatherUseCase,
            provideFakeCoroutinesDispatcherProvider(coroutinesTestRule.testDispatcher)
        )
        weatherUiModelObserver = underTest.weatherUiModel.test()
        errorObserver = underTest.error.test()
        cityObserver = underTest.city.test()
    }

    @Test
    fun `verify get weather success`() = coroutinesTestRule.runBlockingTest {
        loadWeatherUseCase.mockReturnResult {
            successResult(WeatherResponse(Main(0.0, 0), "city"))
        }

        underTest.setCity("city")
        underTest.unit = Units.CELSIUS
        underTest.loadWeather()

        errorObserver.assertNullValue()
        cityObserver.assertHasValue()
        cityObserver.assertValue("city")
        weatherUiModelObserver.assertHasValue()
        weatherUiModelObserver.assertValue(WeatherUiModel("city", "0.0", "0"))

        coVerify(exactly = 1) { loadWeatherUseCase(any()) }
    }

    @Test
    fun `verify set city`() = coroutinesTestRule.runBlockingTest {
        underTest.setCity("city")

        cityObserver.assertHasValue()
        cityObserver.assertValue("city")
        errorObserver.assertNoValue()
        weatherUiModelObserver.assertNoValue()

        coVerify(exactly = 0) { loadWeatherUseCase(any()) }
    }

    @Test
    fun `verify set unit`() = coroutinesTestRule.runBlockingTest {
        underTest.unit = Units.CELSIUS

        errorObserver.assertNoValue()
        cityObserver.assertNoValue()
        weatherUiModelObserver.assertNoValue()

        coVerify(exactly = 0) { loadWeatherUseCase(any()) }
    }

    @Test
    fun `verify failed`() = coroutinesTestRule.runBlockingTest {
        val exception = Exception()
        loadWeatherUseCase.mockReturnResult {
            failResult(exception)
        }

        underTest.setCity("city")
        underTest.loadWeather()

        weatherUiModelObserver.assertNullValue()
        cityObserver.assertHasValue()
        cityObserver.assertValue("city")
        errorObserver.assertHasValue()
        errorObserver.assertValue(exception)

        coVerify(exactly = 1) { loadWeatherUseCase(any()) }
    }
}