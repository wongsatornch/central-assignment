package com.h2osolid.assignment.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.h2osolid.assignment.CoroutinesTestRule
import org.junit.Before
import org.junit.Rule

abstract class BaseViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var coroutinesTestRule = CoroutinesTestRule()

    @Before
    fun setUp() {
        setUpTest()
    }

    protected abstract fun setUpTest()
}