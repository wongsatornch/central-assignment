package com.h2osolid.assignment.data.di

import com.h2osolid.assignment.data.RepositoryImpl
import com.h2osolid.assignment.domain.Repository
import org.koin.dsl.module

val repositoryModule = module {
    single<Repository> { RepositoryImpl(get()) }
}