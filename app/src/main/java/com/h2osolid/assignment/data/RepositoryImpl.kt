package com.h2osolid.assignment.data

import com.h2osolid.assignment.core.api.WeatherApi
import com.h2osolid.assignment.core.model.successOr
import com.h2osolid.assignment.domain.Repository
import com.h2osolid.assignment.model.ForecastResponse
import com.h2osolid.assignment.model.WeatherResponse
import com.h2osolid.assignment.utils.toResult

class RepositoryImpl(
    private val weatherApi: WeatherApi
) : Repository {

    override suspend fun getWeather(
        query: String,
        units: String
    ): WeatherResponse? {
        return weatherApi.getWeather(query, units)
            .toResult()
            .successOr(null)
    }

    override suspend fun getForecast(query: String, units: String): ForecastResponse? {
        return weatherApi.getForecast(query, units)
            .toResult()
            .successOr(null)
    }
}