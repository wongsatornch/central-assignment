package com.h2osolid.assignment.data.di

import org.koin.core.context.loadKoinModules

object DataComponent {
    fun init() = loadKoinModules(listOf(repositoryModule))
}