package com.h2osolid.assignment.data.exception

class FormatterNullPointerException : Exception {
    constructor() : super()
    constructor(message: String) : super(message)
}