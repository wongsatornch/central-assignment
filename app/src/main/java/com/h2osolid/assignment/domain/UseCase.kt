package com.h2osolid.assignment.domain

import android.util.Log
import com.h2osolid.assignment.core.model.Result

abstract class UseCase<in P, R> {
    suspend operator fun invoke(params: P): Result<R> {
        return try {
            Result.Success(execute(params))
        } catch (e: Exception) {
            Log.e(this::class.simpleName, "Exception", e)
            Result.Failure(e)
        }
    }

    @Throws(RuntimeException::class)
    protected abstract suspend fun execute(params: P): R
}