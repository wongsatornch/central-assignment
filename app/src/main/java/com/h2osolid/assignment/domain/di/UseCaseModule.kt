package com.h2osolid.assignment.domain.di

import com.h2osolid.assignment.domain.forecast.LoadForecastUseCase
import com.h2osolid.assignment.domain.weather.LoadWeatherUseCase
import org.koin.dsl.module

val useCaseModule = module {
    factory { LoadWeatherUseCase(get()) }
    factory { LoadForecastUseCase(get()) }
}