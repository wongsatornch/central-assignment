package com.h2osolid.assignment.domain.forecast

import com.h2osolid.assignment.domain.Repository
import com.h2osolid.assignment.domain.UseCase
import com.h2osolid.assignment.model.ForecastResponse
import com.h2osolid.assignment.model.Units

class LoadForecastUseCase(
    private val repository: Repository
) : UseCase<LoadForecastUseCase.Params, ForecastResponse?>() {

    override suspend fun execute(params: Params): ForecastResponse? {
        if (params.query == null || params.query.isEmpty()) throw NullPointerException("query cannot be null.")
        if (params.unit == null) throw NullPointerException("unit cannot be null")
        return repository.getForecast(
            params.query,
            params.unit.value
        )
    }

    data class Params(
        val query: String?,
        val unit: Units?
    )
}