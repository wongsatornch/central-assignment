package com.h2osolid.assignment.domain.weather

import com.h2osolid.assignment.domain.Repository
import com.h2osolid.assignment.domain.UseCase
import com.h2osolid.assignment.model.Units
import com.h2osolid.assignment.model.WeatherResponse

class LoadWeatherUseCase(
    private val repository: Repository
) : UseCase<LoadWeatherUseCase.Params, WeatherResponse?>() {

    override suspend fun execute(params: Params): WeatherResponse? {
        if (params.query == null || params.query.isBlank()) throw NullPointerException("query cannot be null.")
        if (params.unit == null) throw NullPointerException("unit cannot be null")
        return repository.getWeather(
            params.query,
            params.unit.value
        )
    }

    data class Params(
        val query: String?,
        val unit: Units?
    )
}