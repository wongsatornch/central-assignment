package com.h2osolid.assignment.domain

import com.h2osolid.assignment.model.ForecastResponse
import com.h2osolid.assignment.model.Units
import com.h2osolid.assignment.model.WeatherResponse

interface Repository {
    suspend fun getWeather(
        query: String,
        units: String = Units.CELSIUS.value
    ): WeatherResponse?

    suspend fun getForecast(
        query: String,
        units: String = Units.CELSIUS.value
    ): ForecastResponse?
}