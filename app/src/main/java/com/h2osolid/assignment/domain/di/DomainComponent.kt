package com.h2osolid.assignment.domain.di

import org.koin.core.context.loadKoinModules

object DomainComponent {
    fun init() = loadKoinModules(listOf(useCaseModule))
}