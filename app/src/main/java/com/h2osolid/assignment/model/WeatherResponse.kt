package com.h2osolid.assignment.model

import com.google.gson.annotations.SerializedName

data class WeatherResponse(
    @SerializedName("main") val main: Main?,
    @SerializedName("name") val name: String?
)