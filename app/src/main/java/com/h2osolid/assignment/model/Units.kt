package com.h2osolid.assignment.model

enum class Units(val value: String) {
    CELSIUS("metric") {
        override fun toString(): String {
            return "°C"
        }
    },
    FAHRENHEIT("imperial") {
        override fun toString(): String {
            return "°F"
        }
    }
}