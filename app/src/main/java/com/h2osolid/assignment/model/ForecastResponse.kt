package com.h2osolid.assignment.model

import com.google.gson.annotations.SerializedName

data class ForecastResponse(
    @SerializedName("list") val list: List<Forecast> = emptyList()
)