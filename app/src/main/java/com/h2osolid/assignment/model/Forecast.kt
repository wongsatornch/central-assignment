package com.h2osolid.assignment.model

import com.google.gson.annotations.SerializedName

data class Forecast(
    @SerializedName("main") var main: Main?,
    @SerializedName("name") var name: String?,
    @SerializedName("dt_txt") var date: String
)