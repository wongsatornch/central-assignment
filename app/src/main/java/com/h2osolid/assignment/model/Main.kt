package com.h2osolid.assignment.model

import com.google.gson.annotations.SerializedName

data class Main(
    @SerializedName("temp") val temp: Double?,
    @SerializedName("humidity") val humidity: Int?
)