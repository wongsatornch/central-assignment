package com.h2osolid.assignment.core.di

import com.google.gson.Gson
import com.h2osolid.assignment.core.interactor.CoroutinesDispatcherProvider
import org.koin.dsl.module

val coreModule = module {
    single { Gson() }
    single { HttpLoggingInterceptorBuilder().build() }
    single { ApiErrorInterceptorBuilder().build() }
    single { OkHttpClientBuilder(get(), get()).build() }
    single { RetrofitBuilder(get(), get()).build() }
    single { ApiBuilder(get()).weatherApi() }
    single { CoroutinesDispatcherProvider() }
}