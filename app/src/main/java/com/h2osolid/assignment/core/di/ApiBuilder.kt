package com.h2osolid.assignment.core.di

import com.h2osolid.assignment.core.api.WeatherApi
import retrofit2.Retrofit
import retrofit2.create

class ApiBuilder(
    private val retrofit: Retrofit
) {
    fun weatherApi(): WeatherApi = retrofit.create()
}