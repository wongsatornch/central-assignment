package com.h2osolid.assignment.core.di

import com.h2osolid.assignment.core.ApiErrorInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

class OkHttpClientBuilder(
    private val httpLoggingInterceptor: HttpLoggingInterceptor,
    private val apiErrorInterceptor: ApiErrorInterceptor,
) {
    fun build(): OkHttpClient {
        val builder = OkHttpClient.Builder()
        builder.addInterceptor(httpLoggingInterceptor)
        builder.addInterceptor(apiErrorInterceptor)
        return builder.build()
    }
}