package com.h2osolid.assignment.core

import com.h2osolid.assignment.utils.postValueSingleEvent
import okhttp3.Interceptor
import okhttp3.Response


class ApiErrorInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        when (response.code) {
            400, 404 -> {
                ApiErrorLiveData.get().postValueSingleEvent("${response.code}: ${response.message}")
            }
        }
        return response
    }
}