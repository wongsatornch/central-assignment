package com.h2osolid.assignment.core.di

import okhttp3.logging.HttpLoggingInterceptor

class HttpLoggingInterceptorBuilder {
    fun build(): HttpLoggingInterceptor {
        val httpLoggingInterceptor = HttpLoggingInterceptor()
        httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BASIC
        return httpLoggingInterceptor
    }
}