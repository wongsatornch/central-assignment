package com.h2osolid.assignment.core.api

import com.h2osolid.assignment.model.ForecastResponse
import com.h2osolid.assignment.model.Units
import com.h2osolid.assignment.model.WeatherResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("weather")
    suspend fun getWeather(
        @Query("q") query: String,
        @Query("units") units: String = Units.CELSIUS.value,
        @Query("appid") appId: String = "a3e8fc272f53fd62d0878f9f318b49ff"
    ): Response<WeatherResponse>

    @GET("forecast")
    suspend fun getForecast(
        @Query("q") query: String,
        @Query("units") units: String = Units.CELSIUS.value,
        @Query("appid") appId: String = "a3e8fc272f53fd62d0878f9f318b49ff"
    ): Response<ForecastResponse>
}