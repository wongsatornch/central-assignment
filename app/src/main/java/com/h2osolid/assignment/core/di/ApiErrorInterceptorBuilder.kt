package com.h2osolid.assignment.core.di

import com.h2osolid.assignment.core.ApiErrorInterceptor

class ApiErrorInterceptorBuilder {
    fun build() = ApiErrorInterceptor()
}