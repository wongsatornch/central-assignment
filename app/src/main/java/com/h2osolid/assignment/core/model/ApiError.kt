package com.h2osolid.assignment.core.model

sealed class ApiError(open val message: String?) {
    data class NotFoundError(override val message: String?) : ApiError(message)
    data class Others(override val message: String?) : ApiError(message)
}
