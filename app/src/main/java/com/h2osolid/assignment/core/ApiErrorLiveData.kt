package com.h2osolid.assignment.core

import androidx.annotation.MainThread
import androidx.lifecycle.MutableLiveData
import com.h2osolid.assignment.core.model.ApiError

class ApiErrorLiveData : MutableLiveData<String?>() {
    companion object {
        private lateinit var instance: ApiErrorLiveData

        @MainThread
        fun get(): ApiErrorLiveData {
            instance = if (::instance.isInitialized) instance else ApiErrorLiveData()
            return instance
        }
    }
}