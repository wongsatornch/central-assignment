package com.h2osolid.assignment.ui.weather

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.h2osolid.assignment.core.interactor.CoroutinesDispatcherProvider
import com.h2osolid.assignment.core.model.Result
import com.h2osolid.assignment.core.model.successOr
import com.h2osolid.assignment.domain.weather.LoadWeatherUseCase
import com.h2osolid.assignment.model.Units
import com.h2osolid.assignment.model.WeatherResponse
import com.h2osolid.assignment.ui.model.mapper.WeatherToUiMapper
import com.h2osolid.assignment.utils.toSingleEvent
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class WeatherViewModel(
    private val loadWeatherUseCase: LoadWeatherUseCase,
    private val coroutinesDispatcherProvider: CoroutinesDispatcherProvider
) : ViewModel() {

    private val _weatherUiModel = MutableLiveData<Result<WeatherResponse?>>()
    val weatherUiModel get() = _weatherUiModel.map(::mapWeatherResultToUiModel).toSingleEvent()
    val error get() = _weatherUiModel.map(::mapWeatherResultToException).toSingleEvent()

    private val _city = MutableLiveData<String?>()
    val city get() = _city.toSingleEvent()

    var unit: Units? = null

    fun loadWeather() {
        val city = _city.value ?: return
        viewModelScope.launch {
            _weatherUiModel.value = withContext(coroutinesDispatcherProvider.io) {
                loadWeatherUseCase(LoadWeatherUseCase.Params(city, unit))
            }
        }
    }

    fun getCity(): String? = _city.value
    fun setCity(city: String) {
        _city.value = city
    }

    private fun mapWeatherResultToUiModel(result: Result<WeatherResponse?>) =
        result.successOr(null)?.run(WeatherToUiMapper::map)

    private fun mapWeatherResultToException(result: Result<WeatherResponse?>) =
        (result as? Result.Failure)?.exception
}