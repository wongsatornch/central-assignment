package com.h2osolid.assignment.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.h2osolid.assignment.model.Units

class MainViewModel : ViewModel() {

    private val _unit = MutableLiveData<Units>()
    val unit: LiveData<Units> get() = _unit

    init {
        _unit.postValue(Units.CELSIUS)
    }

    fun changeUnit() {
        val unit = if (_unit.value != Units.CELSIUS) Units.CELSIUS else Units.FAHRENHEIT
        _unit.postValue(unit)
    }
}