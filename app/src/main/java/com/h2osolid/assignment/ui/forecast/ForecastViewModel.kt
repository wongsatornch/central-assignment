package com.h2osolid.assignment.ui.forecast

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import com.h2osolid.assignment.core.interactor.CoroutinesDispatcherProvider
import com.h2osolid.assignment.core.model.Result
import com.h2osolid.assignment.core.model.successOr
import com.h2osolid.assignment.domain.forecast.LoadForecastUseCase
import com.h2osolid.assignment.model.ForecastResponse
import com.h2osolid.assignment.model.Units
import com.h2osolid.assignment.ui.model.ForecastUiModel
import com.h2osolid.assignment.ui.model.mapper.ForecastToUiMapper
import com.h2osolid.assignment.utils.toSingleEvent
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ForecastViewModel(
    private val loadForecastUseCase: LoadForecastUseCase,
    private val coroutinesDispatcherProvider: CoroutinesDispatcherProvider
) : ViewModel() {

    private val _weathers = MutableLiveData<Result<ForecastResponse?>>()
    val weathers get() = _weathers.map(::mapWeathersResultToUiModel).toSingleEvent()
    val error get() = _weathers.map(::mapWeathersResultToException).toSingleEvent()

    var city: String? = null
    var unit: Units? = null

    fun loadForecast() {
        viewModelScope.launch {
            _weathers.value = withContext(coroutinesDispatcherProvider.io) {
                loadForecastUseCase(
                    LoadForecastUseCase.Params(city, unit)
                )
            }
        }
    }

    private fun mapWeathersResultToUiModel(result: Result<ForecastResponse?>) =
        result.successOr(null)
            ?.list
            ?.map(ForecastToUiMapper::map)
            ?.onEach(::setUnit)

    private fun mapWeathersResultToException(result: Result<ForecastResponse?>) =
        (result as? Result.Failure)?.exception

    private fun setUnit(data: ForecastUiModel) {
        data.unit = unit
    }
}