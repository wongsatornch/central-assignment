package com.h2osolid.assignment.ui.forecast

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.h2osolid.assignment.ui.model.ForecastUiModel

class ForecastAdapter : ListAdapter<ForecastUiModel, RecyclerView.ViewHolder>(DiffUtils()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return ForecastItemViewHolder(parent, false)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ForecastItemViewHolder) {
            holder.bind(getItem(position))
        }
    }

    class DiffUtils : DiffUtil.ItemCallback<ForecastUiModel>() {
        override fun areItemsTheSame(oldItem: ForecastUiModel, newItem: ForecastUiModel): Boolean =
            oldItem.date == newItem.date

        override fun areContentsTheSame(oldItem: ForecastUiModel, newItem: ForecastUiModel): Boolean =
            oldItem == newItem

    }
}