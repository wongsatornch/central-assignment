package com.h2osolid.assignment.ui

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import com.h2osolid.assignment.core.ApiErrorLiveData

abstract class BaseFragment : Fragment() {

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        observeViewModel()

        ApiErrorLiveData.get().observe(viewLifecycleOwner, ::observeApiError)
    }

    abstract fun observeViewModel()

    private fun observeApiError(message: String?) {
        message ?: return
        Snackbar.make(requireView(), message, Snackbar.LENGTH_SHORT).show()
    }

    protected fun displayError(error: Throwable?) {
        error ?: return
        Snackbar.make(requireView(), error.message.toString(), Snackbar.LENGTH_SHORT).show()
    }

    protected open fun hideKeyboard(displayView: View? = null) {
        (requireContext().getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)
            ?.hideSoftInputFromWindow((displayView ?: view)?.windowToken, 0)
    }
}