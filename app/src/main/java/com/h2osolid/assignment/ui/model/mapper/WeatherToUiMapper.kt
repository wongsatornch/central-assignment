package com.h2osolid.assignment.ui.model.mapper

import com.h2osolid.assignment.model.WeatherResponse
import com.h2osolid.assignment.ui.model.WeatherUiModel

object WeatherToUiMapper : Mapper<WeatherResponse?, WeatherUiModel> {
    override fun map(from: WeatherResponse?): WeatherUiModel {
        return WeatherUiModel(
            from?.name ?: "",
            from?.main?.temp.toString(),
            from?.main?.humidity.toString()
        )
    }
}