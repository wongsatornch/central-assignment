package com.h2osolid.assignment.ui.forecast

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.h2osolid.assignment.databinding.FragmentForecastBinding
import com.h2osolid.assignment.model.Units
import com.h2osolid.assignment.ui.BaseFragment
import com.h2osolid.assignment.ui.MainViewModel
import com.h2osolid.assignment.ui.model.ForecastUiModel
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class ForecastFragment : BaseFragment() {

    private var _binding: FragmentForecastBinding? = null
    private val binding get() = _binding!!

    private val mainViewModel by sharedViewModel<MainViewModel>()
    private val viewModel by viewModel<ForecastViewModel>()
    private val args by navArgs<ForecastFragmentArgs>()

    private val adapter by lazy { ForecastAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentForecastBinding.inflate(layoutInflater, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        setupRecyclerView()
        viewModel.city = args.city
    }

    private fun setupRecyclerView() {
        with(binding.recyclerView) {
            layoutManager = LinearLayoutManager(context)
            adapter = this@ForecastFragment.adapter
        }
    }

    override fun observeViewModel() {
        mainViewModel.unit.observe(viewLifecycleOwner, ::observeUnit)
        viewModel.weathers.observe(viewLifecycleOwner, ::observeForecast)
        viewModel.error.observe(viewLifecycleOwner, ::displayError)
    }

    private fun observeUnit(unit: Units?) {
        viewModel.unit = unit
        viewModel.loadForecast()
    }

    private fun observeForecast(weathers: List<ForecastUiModel>?) {
        adapter.submitList(weathers)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}