package com.h2osolid.assignment.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.h2osolid.assignment.databinding.ActivityMainBinding
import com.h2osolid.assignment.model.Units
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private val viewModel by viewModel<MainViewModel>()
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.toolbar)


        binding.fab.setOnClickListener {
            viewModel.changeUnit()
        }

        viewModel.unit.observe(this, ::observeUnit)
    }

    private fun observeUnit(unit: Units?) {
        binding.fab.text = if (unit != Units.CELSIUS) {
            Units.CELSIUS.toString()
        } else {
            Units.FAHRENHEIT.toString()
        }
    }
}