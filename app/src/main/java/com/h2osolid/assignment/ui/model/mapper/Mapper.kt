package com.h2osolid.assignment.ui.model.mapper

interface Mapper<in From, out To> {
    fun map(from: From): To
}