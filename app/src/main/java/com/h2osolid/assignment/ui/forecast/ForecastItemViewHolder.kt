package com.h2osolid.assignment.ui.forecast

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.h2osolid.assignment.databinding.HolderForecastItemBinding
import com.h2osolid.assignment.ui.model.ForecastUiModel
import com.h2osolid.assignment.utils.formatDate

class ForecastItemViewHolder(
    private val binding: HolderForecastItemBinding
) : RecyclerView.ViewHolder(binding.root) {

    companion object {
        operator fun invoke(parent: ViewGroup, attachToRoot: Boolean) =
            HolderForecastItemBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                attachToRoot
            ).run(::ForecastItemViewHolder)
    }

    fun bind(data: ForecastUiModel?) {
        data ?: return
        binding.tvTemperature.text = "${data.temp}${data.unit}"
        binding.tvHumidity.text = "Humidity : ${data.humidity}%"
        binding.tvDate.text = data.date.formatDate()
    }
}