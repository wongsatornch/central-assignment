package com.h2osolid.assignment.ui.model

import com.h2osolid.assignment.model.Units

data class ForecastUiModel(
    val temp: String,
    val humidity: String,
    val date: String,
    var unit: Units? = null
)