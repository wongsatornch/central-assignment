package com.h2osolid.assignment.ui.model

data class WeatherUiModel(
    val city: String,
    val temp: String,
    val humidity: String
)