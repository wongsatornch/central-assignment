package com.h2osolid.assignment.ui.weather

import android.os.Bundle
import android.text.Editable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.h2osolid.assignment.databinding.FragmentWeatherBinding
import com.h2osolid.assignment.model.Units
import com.h2osolid.assignment.ui.BaseFragment
import com.h2osolid.assignment.ui.MainViewModel
import com.h2osolid.assignment.ui.model.WeatherUiModel
import com.h2osolid.assignment.utils.doAfterTextChanged
import org.koin.android.viewmodel.ext.android.sharedViewModel
import org.koin.android.viewmodel.ext.android.viewModel

class WeatherFragment : BaseFragment() {

    private var _binding: FragmentWeatherBinding? = null
    private val binding get() = _binding!!

    private val mainViewModel by sharedViewModel<MainViewModel>()
    private val viewModel by viewModel<WeatherViewModel>()
    private val unit get() = mainViewModel.unit.value

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentWeatherBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.edtCity.doAfterTextChanged(::afterTextChanged)
        binding.btnForecast.setOnClickListener {
            val direction = WeatherFragmentDirections.toForecastFragment(viewModel.getCity() ?: "")
            findNavController().navigate(direction)
            hideKeyboard(it)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (viewModel.getCity() == null) {
            binding.edtCity.setText("bangkok")
        }
    }

    override fun observeViewModel() {
        mainViewModel.unit.observe(viewLifecycleOwner, ::observeUnit)
        viewModel.weatherUiModel.observe(viewLifecycleOwner, ::observeUiModel)
        viewModel.city.observe(viewLifecycleOwner, ::observeCity)
        viewModel.error.observe(viewLifecycleOwner, ::displayError)
    }

    private fun observeUnit(unit: Units?) {
        viewModel.unit = unit
        loadWeather()
    }

    private fun observeUiModel(weatherUiModel: WeatherUiModel?) {
        weatherUiModel ?: return
        binding.tvCity.text = weatherUiModel.city
        binding.tvTemp.text = "temperature: ${weatherUiModel.temp}"
        binding.tvTempUnit.text = unit.toString()
        binding.tvHumidity.text = "humidity: ${weatherUiModel.humidity}%"
    }

    @Suppress("UNUSED_PARAMETER")
    private fun observeCity(city: String?) {
        loadWeather()
    }

    private fun loadWeather() {
        viewModel.loadWeather()
    }

    private fun afterTextChanged(text: Editable?) {
        viewModel.setCity(text.toString())
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}