package com.h2osolid.assignment.ui.model.mapper

import com.h2osolid.assignment.model.Forecast
import com.h2osolid.assignment.ui.model.ForecastUiModel

object ForecastToUiMapper : Mapper<Forecast, ForecastUiModel> {
    override fun map(from: Forecast): ForecastUiModel {
        return ForecastUiModel(
            temp = from.main?.temp.toString(),
            humidity = from.main?.humidity.toString(),
            date = from.date
        )
    }
}