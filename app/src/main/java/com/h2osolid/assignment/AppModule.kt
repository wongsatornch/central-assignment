package com.h2osolid.assignment

import com.h2osolid.assignment.ui.MainViewModel
import com.h2osolid.assignment.ui.forecast.ForecastViewModel
import com.h2osolid.assignment.ui.weather.WeatherViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    viewModel { MainViewModel() }
    viewModel { WeatherViewModel(get(), get()) }
    viewModel { ForecastViewModel(get(), get()) }
}