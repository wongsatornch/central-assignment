package com.h2osolid.assignment.utils

import android.annotation.SuppressLint
import com.h2osolid.assignment.data.exception.FormatterNullPointerException
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
object DateFormatter {
    private val formatter = SimpleDateFormat("HH:mm EEE, d MMM")
    fun format(date: Date?): String =
        if (date != null) formatter.format(date) else throw FormatterNullPointerException("Date cannot be null")
}

@SuppressLint("SimpleDateFormat")
object StringFormatter {
    private val formatter = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    fun parseToDate(date: String?): Date? =
        if (date != null) formatter.parse(date) else throw FormatterNullPointerException("String date cannot be null")
}

fun String?.formatDate(): String {
    return try {
        if (this != null) {
            val date = StringFormatter.parseToDate(this)
            DateFormatter.format(date)
        } else {
            ""
        }
    } catch (e: FormatterNullPointerException) {
        this ?: ""
    }
}