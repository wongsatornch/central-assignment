package com.h2osolid.assignment.utils

import android.text.Editable
import android.widget.EditText
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.h2osolid.assignment.core.model.Result
import com.hadilq.liveevent.LiveEvent
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import retrofit2.HttpException
import retrofit2.Response
import java.util.*

fun <R> Response<R>.toResult(): Result<R?> {
    return when {
        isSuccessful -> Result.Success(body())
        else -> Result.Failure(HttpException(this))
    }
}

fun <T> MutableLiveData<T?>.postValueSingleEvent(error: T) {
    CoroutineScope(Dispatchers.Main).launch {
        postValue(error)
        delay(500L)
        postValue(null)
    }
}

fun <T> LiveData<T>.toSingleEvent(): LiveEvent<T> {
    val result = LiveEvent<T>()
    result.addSource(this) {
        result.value = it
    }
    return result
}


fun EditText.doAfterTextChanged(
    listener: (text: Editable?) -> Unit,
    delayTime: Long = 1_000
) {
    var timer: Timer? = null

    this.doAfterTextChanged { text ->
        timer?.cancel()
        timer = Timer()
        timer?.schedule(
            object : TimerTask() {
                override fun run() {
                    CoroutineScope(Dispatchers.Main).launch {
                        listener(text)
                    }
                }
            },
            delayTime
        )
    }
}