package com.h2osolid.assignment

import android.app.Application
import com.h2osolid.assignment.core.di.CoreComponent
import com.h2osolid.assignment.data.di.DataComponent
import com.h2osolid.assignment.domain.di.DomainComponent
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class MainApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@MainApplication)
            modules(appModule)
        }
        CoreComponent.init()
        DomainComponent.init()
        DataComponent.init()
    }
}